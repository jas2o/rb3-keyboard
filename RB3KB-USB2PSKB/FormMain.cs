﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RB3KB_USB2PSKB {
    public partial class FormMain : Form {

        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr point);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo); //Didn't work for Phase Shift

        private static USBKB keyboard = null;

        Thread thread = null;
        private bool LEDAnimations = true;

        public FormMain() {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e) {
        }

        private void Start() {
            keyboard = new USBKB();
        }

        private void End() {
            if(keyboard != null)
                keyboard.Close();
        }

        private void USB2MIDI() {
            if (LEDAnimations)
                keyboard.SetLED(1, true);

            //bool holdDpad = false;
            bool holdSlider = false;

            KBData previous = keyboard.Read();
            while (true) {
                KBData data = keyboard.Read();

                //Console Buttons
                ControlKey(0x08, 0x0E, previous.SelectMinus, data.SelectMinus); //Backspace
                ControlKey(0x09, 0x0F, previous.StartPlus, data.StartPlus); //Tab
                ControlKey(0x31, 0x02, previous.CrossA, data.CrossA); //1
                ControlKey(0x32, 0x03, previous.CircleB, data.CircleB);
                ControlKey(0x33, 0x04, previous.Triangle2, data.Triangle2);
                ControlKey(0x34, 0x05, previous.Square1, data.Square1);
                ControlKey(0x35, 0x06, previous.Key[24], data.Key[24]); //5
                ControlKey(0x08, 0x0E, previous.Overdrive, data.Overdrive); //Backspace

                //Arrow Keys
                if (data.Dpad != previous.Dpad) {
                    if (previous.Dpad != 0x08) {
                        if (previous.Dpad == 0)
                            ControlKey(0x26, 0xC8, false); //Up
                        if (previous.Dpad == 2)
                            ControlKey(0x27, 0xCD, false); //Right
                        if (previous.Dpad == 4)
                            ControlKey(0x28, 0xD0, false); //Down
                        if (previous.Dpad == 6)
                            ControlKey(0x25, 0xCB, false); //Left
                    }

                    if(data.Dpad == 0)
                        ControlKey(0x26, 0xC8, true); //Up
                    if (data.Dpad == 2)
                        ControlKey(0x27, 0xCD, true); //Right
                    if (data.Dpad == 4)
                        ControlKey(0x28, 0xD0, true); //Down
                    if (data.Dpad == 6)
                        ControlKey(0x25, 0xCB, true); //Left
                }

                //Below have automatic enter pressing on new keydowns
                //Left side
                int down = 0;
                down += ControlKey(0x31, 0x02, previous.Key[0], data.Key[0]); //1
                down += ControlKey(0x32, 0x03, previous.Key[2], data.Key[2]);
                down += ControlKey(0x33, 0x04, previous.Key[4], data.Key[4]);
                down += ControlKey(0x34, 0x05, previous.Key[5], data.Key[5]);
                down += ControlKey(0x35, 0x06, previous.Key[7], data.Key[7]); //5
                //Right side
                down += ControlKey(0x31, 0x02, previous.Key[12], data.Key[12]); //1
                down += ControlKey(0x32, 0x03, previous.Key[14], data.Key[14]);
                down += ControlKey(0x33, 0x04, previous.Key[16], data.Key[16]);
                down += ControlKey(0x34, 0x05, previous.Key[17], data.Key[17]);
                down += ControlKey(0x35, 0x06, previous.Key[19], data.Key[19]); //5

                //Enter
                if(down > 0 || (!previous.Pedal && data.Pedal) || (!previous.Key[11] && data.Key[11]) || (!previous.Key[13] && data.Key[13]) || (!previous.Key[15] && data.Key[15]) || (!previous.Key[21] && data.Key[21])) {
                    ControlKey(0x0D, 0x1C, true);
                    ControlKey(0x0D, 0x1C, false);
                }

                //Delete
                if (data.Slider != 0 && !holdSlider) {
                    holdSlider = true;
                    ControlKey(0x2E, 0xD3, true);
                } else if (data.Slider == 0 && holdSlider) {
                    ControlKey(0x2E, 0xD3, false);
                    holdSlider = false;
                }

                previous = data;
            }
        }

        private void ControlKey(byte key, byte make, bool down) {
            if(down)
                keybd_event(key, make, 0, 0);
            else
                keybd_event(key, make, 2, 0);
        }

        private int ControlKey(byte key, byte make, bool previous, bool current) {
            if (!previous && current) {
                keybd_event(key, make, 0, 0);
                return 1;
            } else if (previous && !current) {
                keybd_event(key, make, 2, 0);
            }
            return 0;
        }

        private void btnRunLoop_Click(object sender, EventArgs e) {
            if (thread == null || !thread.IsAlive) {
                Start();

                if (keyboard.CanUse()) {
                    thread = new Thread(() => USB2MIDI());
                    thread.Start();

                    btnRunLoop.Text = "Running";
                } else {
                    End();
                    btnRunLoop.Text = "Keyboard not found - missing filter?";
                }
            } else {
                thread.Abort();
                End();

                btnRunLoop.Text = "Doing Nothing";
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            if(thread != null)
                thread.Abort();
            End();
        }
    }
}
