﻿using System.Threading;

using LibUsbDotNet;
using LibUsbDotNet.Main;

namespace RB3KB_USB2MIDI {
    class USBKB {

        private static UsbDeviceFinder usbFinderKBPS3 = new UsbDeviceFinder(0x12ba, 0x2330);
        private static UsbDeviceFinder usbFinderKBWii = new UsbDeviceFinder(0x1bad, 0x3330);

        private UsbDevice usbDevice;
        private UsbEndpointReader reader;
        private byte LED;
        private bool isWii;

        public USBKB() {
            bool found = InitPS3();
            if (!found)
                isWii = InitWii();
        }

        public void SetLED(byte led, bool remember=false) {
            if (remember)
                LED = led;

            byte[] buffer = { 0, 0, led, 0, 0, 0, 0, 0 };
            UsbSetupPacket setupPacket = new UsbSetupPacket(0x21, 9, 0x0200, 0x00, (short)buffer.Length);
            int lengthTransfered = -1;
            usbDevice.ControlTransfer(ref setupPacket, buffer, buffer.Length, out lengthTransfered);
        }

        private bool InitPS3() {
            usbDevice = UsbDevice.OpenUsbDevice(usbFinderKBPS3);
            if (usbDevice == null)
                return false;

            IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
            if (!ReferenceEquals(wholeUsbDevice, null)) {
                wholeUsbDevice.SetConfiguration(1);
                wholeUsbDevice.ClaimInterface(0);
            }

            reader = usbDevice.OpenEndpointReader(ReadEndpointID.Ep01);

            byte[] msg2 = { 0xE9, 0x00, 0x89, 0x1B, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x89, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE9, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            UsbSetupPacket setupPacket = new UsbSetupPacket(0x21, 0x09, 0x0300, 0x00, (short)msg2.Length);

            int lengthTransfered = -1;
            usbDevice.ControlTransfer(ref setupPacket, msg2, msg2.Length, out lengthTransfered); //Extra features

            return true;
        }

        private bool InitWii() {
            usbDevice = UsbDevice.OpenUsbDevice(usbFinderKBWii);
            if (usbDevice == null)
                return false;

            IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
            if (!ReferenceEquals(wholeUsbDevice, null)) {
                wholeUsbDevice.SetConfiguration(1);
                wholeUsbDevice.ClaimInterface(0);
            }

            reader = usbDevice.OpenEndpointReader(ReadEndpointID.Ep01);

            return true;
        }

        public bool CanUse() {
            return (usbDevice != null);
        }

        public KBData Read() {

            int bytesRead;
            byte[] readBuffer = new byte[27];
            ErrorCode ec = reader.Read(readBuffer, 100, out bytesRead);

            return new KBData(readBuffer);
        }

        public void LEDAnimateLTR() {
            //From what I can tell, it's impossible to have the left two/three lit up at the same time, so can't emulate the MIDI mode animations the same.
            SetLED(1);
            Thread.Sleep(50);
            SetLED(2);
            Thread.Sleep(50);
            SetLED(4);
            Thread.Sleep(50);
            SetLED(8);
            Thread.Sleep(50);
            SetLED(LED);
        }

        public void LEDAnimateRTL() {
            SetLED(8);
            Thread.Sleep(50);
            SetLED(4);
            Thread.Sleep(50);
            SetLED(2);
            Thread.Sleep(50);
            SetLED(1);
            Thread.Sleep(50);
            SetLED(LED);
        }

        public void Close() {
            if (usbDevice != null) {
                if (usbDevice.IsOpen) {
                    IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
                    if (!ReferenceEquals(wholeUsbDevice, null)) wholeUsbDevice.ReleaseInterface(0);
                    usbDevice.Close();
                }
                usbDevice = null;
                UsbDevice.Exit();
            }
        }

    }
}
