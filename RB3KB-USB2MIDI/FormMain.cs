﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Midi;

namespace RB3KB_USB2MIDI {
    public partial class FormMain : Form {

        #region Dictionary Program
        //Numbers are +1 of the enum in the Midi library.
        private static Dictionary<int, string> dictionaryProgram = new Dictionary<int, string>() {
            { 1, "Acoustic Grand Piano" },
            { 2, "Bright Acoustic Piano" },
            { 3, "Electric Grand Piano" },
            { 4, "Honky-tonk Piano" },
            { 5, "Electric Piano 1" },
            { 6, "Electric Piano 2" },
            { 7, "Harpsichord" },
            { 8, "Clavi" },
            { 9, "Celesta" },
            { 10, "Glockenspiel" },
            { 11, "Music Box" },
            { 12, "Vibraphone" },
            { 13, "Marimba" },
            { 14, "Xylophone" },
            { 15, "Tubular Bells" },
            { 16, "Dulcimer" },
            { 17, "Drawbar Organ" },
            { 18, "Percussive Organ" },
            { 19, "Rock Organ" },
            { 20, "Church Organ" },
            { 21, "Reed Organ" },
            { 22, "Accordion" },
            { 23, "Harmonica" },
            { 24, "Tango Accordion" },
            { 25, "Acoustic Guitar (Nylon)" },
            { 26, "Acoustic Guitar (Steel)" },
            { 27, "Electric Guitar (Jazz)" },
            { 28, "Electric Guitar (Clean)" },
            { 29, "Electric Guitar (Muted)" },
            { 30, "Overdriven Guitar" },
            { 31, "Distortion Guitar" },
            { 32, "Guitar harmonics" },
            { 33, "Acoustic Bass" },
            { 34, "Electric Bass (Finger)" },
            { 35, "Electric Bass (Pick)" },
            { 36, "Fretless Bass" },
            { 37, "Slap Bass 1" },
            { 38, "Slap Bass 2" },
            { 39, "Synth Bass 1" },
            { 40, "Synth Bass 2" },
            { 41, "Violin" },
            { 42, "Viola" },
            { 43, "Cello" },
            { 44, "Contrabass" },
            { 45, "Tremolo Strings" },
            { 46, "Pizzicato Strings" },
            { 47, "Orchestral Harp" },
            { 48, "Timpani" },
            { 49, "String Ensemble 1" },
            { 50, "String Ensemble 2" },
            { 51, "SynthStrings 1" },
            { 52, "SynthStrings 2" },
            { 53, "Choir Aahs" },
            { 54, "Voice Oohs" },
            { 55, "Synth Voice" },
            { 56, "Orchestra Hit" },
            { 57, "Trumpet" },
            { 58, "Trombone" },
            { 59, "Tuba" },
            { 60, "Muted Trumpet" },
            { 61, "French Horn" },
            { 62, "Brass Section" },
            { 63, "SynthBrass 1" },
            { 64, "SynthBrass 2" },
            { 65, "Soprano Sax" },
            { 66, "Alto Sax" },
            { 67, "Tenor Sax" },
            { 68, "Baritone Sax" },
            { 69, "Oboe" },
            { 70, "English Horn" },
            { 71, "Bassoon" },
            { 72, "Clarinet" },
            { 73, "Piccolo" },
            { 74, "Flute" },
            { 75, "Recorder" },
            { 76, "Pan Flute" },
            { 77, "Blown Bottle" },
            { 78, "Shakuhachi" },
            { 79, "Whistle" },
            { 80, "Ocarina" },
            { 81, "Lead 1 (Square)" },
            { 82, "Lead 2 (Sawtooth)" },
            { 83, "Lead 3 (Calliope)" },
            { 84, "Lead 4 (Chiff)" },
            { 85, "Lead 5 (Charang)" },
            { 86, "Lead 6 (Voice)" },
            { 87, "Lead 7 (Fifths)" },
            { 88, "Lead 8 (Bass + Lead)" },
            { 89, "Pad 1 (New Age)" },
            { 90, "Pad 2 (Warm)" },
            { 91, "Pad 3 (Polysynth)" },
            { 92, "Pad 4 (Choir)" },
            { 93, "Pad 5 (Bowed)" },
            { 94, "Pad 6 (Metallic)" },
            { 95, "Pad 7 (Halo)" },
            { 96, "Pad 8 (Sweep)" },
            { 97, "FX 1 (Rain)" },
            { 98, "FX 2 (Soundtrack)" },
            { 99, "FX 3 (Crystal)" },
            { 100, "FX 4 (Atmosphere)" },
            { 101, "FX 5 (Brightness)" },
            { 102, "FX 6 (Goblins)" },
            { 103, "FX 7 (Echoes)" },
            { 104, "FX 8 (Sci-Fi)" },
            { 105, "Sitar" },
            { 106, "Banjo" },
            { 107, "Shamisen" },
            { 108, "Koto" },
            { 109, "Kalimba" },
            { 110, "Bag pipe" },
            { 111, "Fiddle" },
            { 112, "Shanai" },
            { 113, "Tinkle Bell" },
            { 114, "Agogo" },
            { 115, "Steel Drums" },
            { 116, "Woodblock" },
            { 117, "Taiko Drum" },
            { 118, "Melodic Tom" },
            { 119, "Synth Drum" },
            { 120, "Reverse Cymbal" },
            { 121, "Guitar Fret Noise" },
            { 122, "Breath Noise" },
            { 123, "Seashore" },
            { 124, "Bird Tweet" },
            { 125, "Telephone Ring" },
            { 126, "Helicopter" },
            { 127, "Applause" },
            { 128, "Gunshot" },
        };
        #endregion

        private static OutputDevice MIDI_OUT = null;
        private static USBKB keyboard = null;

        enum PedalMode { Expression, ChannelVolume, FootController };

        Pitch[] map_drum = new Pitch[]{
            Pitch.B1, Pitch.C2, Pitch.D2, Pitch.E2, Pitch.F2, Pitch.B2, Pitch.D3, Pitch.FSharp2, Pitch.ASharp2, Pitch.CSharp3, Pitch.DSharp3, Pitch.F3
        };

        Thread thread = null;
        private int Octave = 4;
        private int Program = 0;
        private bool DrumMapping = false;
        private bool SwapModulationPitchBend = false;
        private bool LEDAnimations = false;
        private bool ModIgnoreZero = false;
        private bool PitchIgnoreZero = false;

        #region Dpad (Drum / Swap Mod and Pitchbend)
        private void chkDrumMapping_CheckedChanged(object sender, EventArgs e) {
            DrumMapping = chkDrumMapping.Checked;
        }

        public void SetDrumMapping(bool set) {
            DrumMapping = set;

            this.Invoke((MethodInvoker)delegate {
                chkDrumMapping.Checked = DrumMapping;
            });
        }

        private void chkSwapModulationPitchBand_CheckedChanged(object sender, EventArgs e) {
            SwapModulationPitchBend = chkSwapModulationPitchBand.Checked;
        }

        public void SetModPitchBend(bool set) {
            SwapModulationPitchBend = set;

            this.Invoke((MethodInvoker)delegate {
                chkSwapModulationPitchBand.Checked = SwapModulationPitchBend;
            });
        }
        #endregion

        #region Set Octave
        public void SetOctave(int octave) {
            octave = Math.Min(octave, 8);
            octave = Math.Max(octave, 0);
            Octave = octave;

            this.Invoke((MethodInvoker)delegate {
                txtOctave.Text = Octave.ToString();
            });
        }

        private void txtOctave_ValueChanged(object sender, EventArgs e) {
            int octave;
            if (Int32.TryParse(txtOctave.Text, out octave))
                SetOctave(octave);
        }
        #endregion

        #region Set Program / Instrument
        public void SetProgram(int program) {
            program = Math.Min(program, 127);
            program = Math.Max(program, 0);
            Program = program;

            if(MIDI_OUT != null && MIDI_OUT.IsOpen)
                MIDI_OUT.SendProgramChange(Channel.Channel1, (Instrument)Program);

            this.Invoke((MethodInvoker)delegate {
                txtInstrumentNum.Text = (Program + 1).ToString();
                ddlInstrument.SelectedItem = dictionaryProgram[Program + 1];
            });
        }

        private void ddlInstrument_SelectedIndexChanged(object sender, EventArgs e) {
            SetProgram(dictionaryProgram.FirstOrDefault(x => x.Value == ddlInstrument.SelectedItem.ToString()).Key - 1);
        }

        private void txtInstrumentNum_ValueChanged(object sender, EventArgs e) {
            int program;
            if (Int32.TryParse(txtInstrumentNum.Text, out program))
                SetProgram(program - 1);
        }
        #endregion

        public FormMain() {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e) {
            foreach (OutputDevice device in OutputDevice.InstalledDevices) {
                ddlMidiOut.Items.Add(device.Name);

                if (device.Name == Properties.Settings.Default.LastUsedMidiOut)
                    ddlMidiOut.SelectedIndex = ddlMidiOut.Items.Count - 1;
            }

            foreach(KeyValuePair<int, string> pair in dictionaryProgram)
                ddlInstrument.Items.Add(pair.Value);

            SetOctave(4);
            SetProgram(0);

            LEDAnimations = chkLEDAnimations.Checked = Properties.Settings.Default.LastUsedLEDAnimations;
        }

        private void Start() {
            MIDI_OUT.Open();
            keyboard = new USBKB();
        }

        private void End() {
            if(keyboard != null)
                keyboard.Close();

            if(MIDI_OUT != null && MIDI_OUT.IsOpen)
                MIDI_OUT.Close();
        }

        private void USB2MIDI() {
            if (LEDAnimations) {
                if(DrumMapping)
                    keyboard.SetLED(8, true);
                else
                    keyboard.SetLED(1, true);
            }

            bool holdPanic = false;
            bool holdDpad = false;
            bool holdPedal = false;
            bool holdProgram = false;
            bool holdOctave = false;

            MIDI_OUT.SendProgramChange(Channel.Channel1, (Instrument)Program);

            Dictionary<int, Tuple<Channel, Pitch>> HeldKey = new Dictionary<int, Tuple<Channel, Pitch>>();

            KBData previous = keyboard.Read();
            while (true) {
                KBData data = keyboard.Read();

                #region Panic: All Notes Off (Select, Home and Start together)
                if(data.SelectMinus && data.Home && data.StartPlus && !holdPanic) {
                    holdPanic = true;
                    int max = (8 * 12) + 25;
                    for (int i = 0; i < max; i++)
                        MIDI_OUT.SendNoteOff(Channel.Channel1, (Pitch)i, 0);
                    for(int i = 0; i < map_drum.Length; i++)
                        MIDI_OUT.SendNoteOff(Channel.Channel10, map_drum[i], 0);
                } else if (!data.SelectMinus && !data.Home && !data.StartPlus && holdPanic) {
                    holdPanic = false;
                }
                #endregion

                #region Dpad (Drum / Swap / Pedal)
                if (data.Dpad == 0 && !holdDpad) { //Up - Drum Mapping
                    holdDpad = true;
                    SetDrumMapping(!DrumMapping);

                    if (LEDAnimations) {
                        if (DrumMapping)
                            keyboard.SetLED(8, true);
                        else
                            keyboard.SetLED(1, true);
                    }
                } else if (data.Dpad == 4 && !holdDpad) { //Down - Swap Modulation / Pitch Bend
                    holdDpad = true;
                    SetModPitchBend(!SwapModulationPitchBend);
                } else if (data.Dpad == 6 && !holdDpad) { //Left - Reset Pitch Bend
                    holdDpad = true;
                    MIDI_OUT.SendPitchBend(Channel.Channel1, 8192);
                } else if (data.Dpad == 2 && !holdDpad) { //Right - Reset Modulation
                    holdDpad = true;
                    MIDI_OUT.SendControlChange(Channel.Channel1, Midi.Control.ModulationWheel, 0);
                } else if (data.Dpad == 8 && holdDpad) {
                    holdDpad = false;
                }

                if (data.Pedal && !holdPedal) {
                    holdPedal = true;
                    MIDI_OUT.SendControlChange(Channel.Channel1, Midi.Control.SustainPedal, 127);
                } else if (!data.Pedal && holdPedal) {
                    MIDI_OUT.SendControlChange(Channel.Channel1, Midi.Control.SustainPedal, 0);
                    holdPedal = false;
                }
                #endregion

                #region Program
                if (data.Triangle2 && !holdProgram) {
                    holdProgram = true;
                    SetProgram(Program+1);
                    if (LEDAnimations)
                        new Thread(() => keyboard.LEDAnimateLTR()).Start();
                } else if (data.CrossA && !holdProgram) {
                    holdProgram = true;
                    SetProgram(Program-1);
                    if (LEDAnimations)
                        new Thread(() => keyboard.LEDAnimateRTL()).Start();
                } else if (!data.Triangle2 && !data.CrossA && holdProgram) {
                    holdProgram = false;
                }
                #endregion

                #region Octave
                if (data.CircleB && !holdOctave) {
                    holdOctave = true;
                    SetOctave(Octave+1);
                    if(LEDAnimations)
                        new Thread(() => keyboard.LEDAnimateLTR()).Start();
                } else if (data.Square1 && !holdOctave) {
                    holdOctave = true;
                    SetOctave(Octave-1);
                    if (LEDAnimations)
                        new Thread(() => keyboard.LEDAnimateRTL()).Start();
                } else if (!data.CircleB && !data.Square1 && holdOctave) {
                    holdOctave = false;
                }
                #endregion

                #region Keys
                for (int i = 0; i < data.Key.Length; i++) {
                    if (data.Key[i] && !previous.Key[i]) {
                        int pitch = (Octave * 12) + i;

                        //Probably only correct when only pressing keys further right of already pressed keys.
                        int velocity = 80;
                        for (int v = data.Velocity.Length - 1; v >= 0; v--) {
                            if (data.Velocity[v] != 0) {
                                velocity = data.Velocity[v];
                                break;
                            }
                        }

                        if (i < map_drum.Length && DrumMapping) {
                            HeldKey.Add(i, new Tuple<Channel, Pitch>(Channel.Channel10, map_drum[i]));
                            MIDI_OUT.SendNoteOn(Channel.Channel10, map_drum[i], velocity);
                        } else {
                            HeldKey.Add(i, new Tuple<Channel, Pitch>(Channel.Channel1, (Pitch)pitch));
                            MIDI_OUT.SendNoteOn(Channel.Channel1, (Pitch)pitch, velocity);
                        }
                    } else if (!data.Key[i] && previous.Key[i] && HeldKey.ContainsKey(i)) {
                        MIDI_OUT.SendNoteOff(HeldKey[i].Item1, HeldKey[i].Item2, 0);
                        HeldKey.Remove(i);
                    }
                }
                #endregion

                #region Slider and Overdrive (Pitch Bend and Modulation)
                if (data.Slider != previous.Slider) {
                    if((data.Overdrive && !SwapModulationPitchBend) || (!data.Overdrive && SwapModulationPitchBend) ) {
                        Console.WriteLine(data.Slider);
                        if(data.Slider == 0 && PitchIgnoreZero) {
                            //Ignore
                        } else if (data.Slider == 50 || data.Slider == 0)
                            MIDI_OUT.SendPitchBend(Channel.Channel1, 8192);
                        else
                            MIDI_OUT.SendPitchBend(Channel.Channel1, (data.Slider * 140));
                    } else {
                        if (data.Slider == 0 && ModIgnoreZero) {
                            //Ignore
                        } else
                            MIDI_OUT.SendControlChange(Channel.Channel1, Midi.Control.ModulationWheel, data.Slider);
                    }
                }
                #endregion

                previous = data;
            }
        }

        private void ddlMidiOut_SelectedIndexChanged(object sender, EventArgs e) {
            MIDI_OUT = OutputDevice.InstalledDevices[ddlMidiOut.SelectedIndex];

            Properties.Settings.Default.LastUsedMidiOut = ddlMidiOut.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        private void btnRunLoop_Click(object sender, EventArgs e) {
            if (thread == null || !thread.IsAlive) {
                if (MIDI_OUT != null) {
                    Start();

                    if (keyboard.CanUse()) {
                        thread = new Thread(() => USB2MIDI());
                        thread.Start();

                        ddlMidiOut.Enabled = false;
                        btnRunLoop.Text = "Running";
                    } else {
                        End();
                        ddlMidiOut.Enabled = true;
                        btnRunLoop.Text = "Keyboard not found - missing filter?";
                    }
                }
            } else {
                thread.Abort();
                End();

                ddlMidiOut.Enabled = true;
                btnRunLoop.Text = "Doing Nothing";
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            if(thread != null)
                thread.Abort();
            End();
        }

        private void chkLEDAnimations_CheckedChanged(object sender, EventArgs e) {
            LEDAnimations = chkLEDAnimations.Checked;

            Properties.Settings.Default.LastUsedLEDAnimations = LEDAnimations;
            Properties.Settings.Default.Save();
        }

        private void chkModPitchIgnoreZero_CheckedChanged(object sender, EventArgs e) {
            ModIgnoreZero = chkModIgnoreZero.Checked;
        }

        private void chkPitchIgnoreZero_CheckedChanged(object sender, EventArgs e) {
            PitchIgnoreZero = chkPitchIgnoreZero.Checked;
        }
    }
}
