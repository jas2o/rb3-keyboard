﻿namespace RB3KB_USB2MIDI {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ddlMidiOut = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRunLoop = new System.Windows.Forms.Button();
            this.ddlInstrument = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOctave = new System.Windows.Forms.NumericUpDown();
            this.txtInstrumentNum = new System.Windows.Forms.NumericUpDown();
            this.chkDrumMapping = new System.Windows.Forms.CheckBox();
            this.chkSwapModulationPitchBand = new System.Windows.Forms.CheckBox();
            this.chkLEDAnimations = new System.Windows.Forms.CheckBox();
            this.chkModIgnoreZero = new System.Windows.Forms.CheckBox();
            this.chkPitchIgnoreZero = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOctave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstrumentNum)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlMidiOut
            // 
            this.ddlMidiOut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlMidiOut.FormattingEnabled = true;
            this.ddlMidiOut.Location = new System.Drawing.Point(6, 19);
            this.ddlMidiOut.Name = "ddlMidiOut";
            this.ddlMidiOut.Size = new System.Drawing.Size(284, 21);
            this.ddlMidiOut.TabIndex = 0;
            this.ddlMidiOut.SelectedIndexChanged += new System.EventHandler(this.ddlMidiOut_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ddlMidiOut);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(297, 52);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MIDI Out";
            // 
            // btnRunLoop
            // 
            this.btnRunLoop.Location = new System.Drawing.Point(18, 70);
            this.btnRunLoop.Name = "btnRunLoop";
            this.btnRunLoop.Size = new System.Drawing.Size(291, 23);
            this.btnRunLoop.TabIndex = 1;
            this.btnRunLoop.Text = "Doing Nothing";
            this.btnRunLoop.UseVisualStyleBackColor = true;
            this.btnRunLoop.Click += new System.EventHandler(this.btnRunLoop_Click);
            // 
            // ddlInstrument
            // 
            this.ddlInstrument.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlInstrument.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ddlInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlInstrument.FormattingEnabled = true;
            this.ddlInstrument.Location = new System.Drawing.Point(156, 129);
            this.ddlInstrument.Name = "ddlInstrument";
            this.ddlInstrument.Size = new System.Drawing.Size(153, 21);
            this.ddlInstrument.Sorted = true;
            this.ddlInstrument.TabIndex = 6;
            this.ddlInstrument.SelectedIndexChanged += new System.EventHandler(this.ddlInstrument_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Octave";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Instrument";
            // 
            // txtOctave
            // 
            this.txtOctave.Location = new System.Drawing.Point(88, 103);
            this.txtOctave.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.txtOctave.Name = "txtOctave";
            this.txtOctave.Size = new System.Drawing.Size(46, 20);
            this.txtOctave.TabIndex = 3;
            this.txtOctave.ValueChanged += new System.EventHandler(this.txtOctave_ValueChanged);
            // 
            // txtInstrumentNum
            // 
            this.txtInstrumentNum.Location = new System.Drawing.Point(88, 129);
            this.txtInstrumentNum.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.txtInstrumentNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtInstrumentNum.Name = "txtInstrumentNum";
            this.txtInstrumentNum.Size = new System.Drawing.Size(46, 20);
            this.txtInstrumentNum.TabIndex = 5;
            this.txtInstrumentNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtInstrumentNum.ValueChanged += new System.EventHandler(this.txtInstrumentNum_ValueChanged);
            // 
            // chkDrumMapping
            // 
            this.chkDrumMapping.AutoSize = true;
            this.chkDrumMapping.Location = new System.Drawing.Point(18, 155);
            this.chkDrumMapping.Name = "chkDrumMapping";
            this.chkDrumMapping.Size = new System.Drawing.Size(111, 17);
            this.chkDrumMapping.TabIndex = 7;
            this.chkDrumMapping.Text = "Drum Map Left 12";
            this.chkDrumMapping.UseVisualStyleBackColor = true;
            this.chkDrumMapping.CheckedChanged += new System.EventHandler(this.chkDrumMapping_CheckedChanged);
            // 
            // chkSwapModulationPitchBand
            // 
            this.chkSwapModulationPitchBand.AutoSize = true;
            this.chkSwapModulationPitchBand.Location = new System.Drawing.Point(18, 178);
            this.chkSwapModulationPitchBand.Name = "chkSwapModulationPitchBand";
            this.chkSwapModulationPitchBand.Size = new System.Drawing.Size(165, 17);
            this.chkSwapModulationPitchBand.TabIndex = 8;
            this.chkSwapModulationPitchBand.Text = "Swap Modulation/Pitch Bend";
            this.chkSwapModulationPitchBand.UseVisualStyleBackColor = true;
            this.chkSwapModulationPitchBand.CheckedChanged += new System.EventHandler(this.chkSwapModulationPitchBand_CheckedChanged);
            // 
            // chkLEDAnimations
            // 
            this.chkLEDAnimations.AutoSize = true;
            this.chkLEDAnimations.Location = new System.Drawing.Point(18, 201);
            this.chkLEDAnimations.Name = "chkLEDAnimations";
            this.chkLEDAnimations.Size = new System.Drawing.Size(101, 17);
            this.chkLEDAnimations.TabIndex = 9;
            this.chkLEDAnimations.Text = "LED Animations";
            this.chkLEDAnimations.UseVisualStyleBackColor = true;
            this.chkLEDAnimations.CheckedChanged += new System.EventHandler(this.chkLEDAnimations_CheckedChanged);
            // 
            // chkModIgnoreZero
            // 
            this.chkModIgnoreZero.AutoSize = true;
            this.chkModIgnoreZero.Location = new System.Drawing.Point(191, 178);
            this.chkModIgnoreZero.Name = "chkModIgnoreZero";
            this.chkModIgnoreZero.Size = new System.Drawing.Size(120, 17);
            this.chkModIgnoreZero.TabIndex = 10;
            this.chkModIgnoreZero.Text = "Modulation Ignore 0";
            this.chkModIgnoreZero.UseVisualStyleBackColor = true;
            this.chkModIgnoreZero.CheckedChanged += new System.EventHandler(this.chkModPitchIgnoreZero_CheckedChanged);
            // 
            // chkPitchIgnoreZero
            // 
            this.chkPitchIgnoreZero.AutoSize = true;
            this.chkPitchIgnoreZero.Location = new System.Drawing.Point(191, 201);
            this.chkPitchIgnoreZero.Name = "chkPitchIgnoreZero";
            this.chkPitchIgnoreZero.Size = new System.Drawing.Size(120, 17);
            this.chkPitchIgnoreZero.TabIndex = 11;
            this.chkPitchIgnoreZero.Text = "Pitch Bend Ignore 0";
            this.chkPitchIgnoreZero.UseVisualStyleBackColor = true;
            this.chkPitchIgnoreZero.CheckedChanged += new System.EventHandler(this.chkPitchIgnoreZero_CheckedChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 230);
            this.Controls.Add(this.chkPitchIgnoreZero);
            this.Controls.Add(this.chkModIgnoreZero);
            this.Controls.Add(this.chkLEDAnimations);
            this.Controls.Add(this.chkSwapModulationPitchBand);
            this.Controls.Add(this.chkDrumMapping);
            this.Controls.Add(this.txtInstrumentNum);
            this.Controls.Add(this.txtOctave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlInstrument);
            this.Controls.Add(this.btnRunLoop);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Rock Band 3 Keyboard USB to MIDI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOctave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstrumentNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlMidiOut;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnRunLoop;
        private System.Windows.Forms.ComboBox ddlInstrument;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txtOctave;
        private System.Windows.Forms.NumericUpDown txtInstrumentNum;
        private System.Windows.Forms.CheckBox chkDrumMapping;
        private System.Windows.Forms.CheckBox chkSwapModulationPitchBand;
        private System.Windows.Forms.CheckBox chkLEDAnimations;
        private System.Windows.Forms.CheckBox chkModIgnoreZero;
        private System.Windows.Forms.CheckBox chkPitchIgnoreZero;
    }
}

